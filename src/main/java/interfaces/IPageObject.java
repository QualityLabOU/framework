package interfaces;

public interface IPageObject {

    boolean isPresent();
}
