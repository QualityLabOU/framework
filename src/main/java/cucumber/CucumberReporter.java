package cucumber;

import com.saucelabs.saucerest.SauceREST;
import gherkin.deps.net.iharder.Base64;
import gherkin.formatter.Formatter;
import gherkin.formatter.NiceAppendable;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.*;
import models.Device;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import utils.TestManager;
import utils.TestManagerException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static utils.TestManager.getInstance;

public class CucumberReporter implements Formatter, Reporter {

    private static final Logger LOG = LogManager.getLogger(CucumberReporter.class);

    private ThreadLocal<List<Result>> results = new ThreadLocal<>();

    private Map<String, Object> featureMap;
    private String uri;
    private List<Map> beforeHooks = new ArrayList<>();

    private boolean inScenarioOutline = false;
    private boolean haveSaucelabs = true;

    private enum Phase {step, match, embedding, output, result}

    public CucumberReporter(Appendable out) {
        if (CucumberAutomationTest.out == null) {
            synchronized (CucumberAutomationTest.class) {
                CucumberAutomationTest.out = new NiceAppendable(out);
            }
        }
    }

    @Override
    public void syntaxError(String s, String s1, List<String> list, String s2, Integer integer) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void uri(String s) {
        this.uri = s;
    }

    @Override
    public void feature(Feature feature) {
        String deviceName = " [" + TestManager.getInstance().getCurrentDevice().getName() + "]";
        Feature updatedFeature = new Feature(feature.getComments(), feature.getTags(), feature.getKeyword(), feature.getName() + deviceName, feature.getDescription(), feature.getLine(), feature.getId());

        featureMap = updatedFeature.toMap();
        String finalUri = uri.split("\\.")[0] + deviceName + uri.split("\\.")[1];
        featureMap.put("uri", finalUri);

        LOG.info("Feature named: " + updatedFeature.getName() + " started");
    }

    @Override
    public void scenarioOutline(ScenarioOutline scenarioOutline) {
        inScenarioOutline = true;
        getFeatureElements().add(scenarioOutline.toMap());
    }

    @Override
    public void examples(Examples examples) {
        getAllExamples().add(examples.toMap());
    }

    @Override
    public void startOfScenarioLifeCycle(Scenario scenario) {
        LOG.info("START OF SCENARIO LIFE");
        inScenarioOutline = false;
        if (((RemoteWebDriver) TestManager.getInstance().getDriver()).getSessionId() == null) {
            Device device = TestManager.getInstance().getCurrentDevice();
            try {
                TestManager.getInstance().setDriver(device);
            } catch (Exception e) {
                LOG.error("Failed to set driver");
            }
        }
    }

    @Override
    public void background(Background background) {
        getFeatureElements().add(background.toMap());
    }

    @Override
    public void scenario(Scenario scenario) {
        getFeatureElements().add(scenario.toMap());
        if (beforeHooks.size() > 0) {
            getFeatureElement().put("before", beforeHooks);
            beforeHooks = new ArrayList<>();
        }

        LOG.info("Scenario named: " + scenario.getName() + " started");
    }

    @Override
    public void step(Step step) {
        if (!inScenarioOutline) {
            getSteps().add(step.toMap());
        }
    }

    @Override
    public void endOfScenarioLifeCycle(Scenario scenario) {
        LOG.info("End of scenario Lifecycle");

        LOG.info("WATCH VIDEO ON SAUCELABS " + getJobLink() + " WATCH VIDEO ON SAUCELABS");
        write(getJobLink());

        try {
            TestManager.getSauceRestApi().stopJob(TestManager.getInstance().getDriverSessionId());
        } catch (TestManagerException e) {
            LOG.info("SauceLabs credentials are missing");
        }

        if (getInstance().getDriver() != null) {
            try {
                getInstance().getDriver().quit();
            } catch (Exception e) {
                LOG.info("Failed to quit driver");
            }
        }
    }

    @Override
    public void done() {
    }

    @Override
    public void close() {
    }

    @Override
    public void eof() {
        CucumberAutomationTest.getFeatures().add(featureMap);
    }

    @Override
    public void before(Match match, Result result) {
        beforeHooks.add(buildHookMap(match, result));
    }

    @Override
    public void result(Result result) {
        List<Result> resultList = results.get();
        if (resultList == null) resultList = new ArrayList<>();
        resultList.add(result);
        results.set(resultList);

        getCurrentStep(Phase.result).put("result", result.toMap());
    }

    @Override
    public void after(Match match, Result result) {
        LOG.info("AFTER");

        for (Result res : results.get()) {
            if (res.getStatus().equals("failed")) {
                LOG.error("Failed because: " + res.getErrorMessage());
                write("Failed because: " + res.getError().getMessage());
                if (haveSaucelabs) sendStatusToSauceLabs(res.getStatus());
                break;
            } else if (res.getStatus().equals("passed")) {
                if (haveSaucelabs) sendStatusToSauceLabs(res.getStatus());
            }
        }

        addHooks(match, result);
        results.get().clear();
    }

    private void sendStatusToSauceLabs(String status) {

        SauceREST sauceApi = null;

        // Get API and JobLink
        try {
            sauceApi = TestManager.getSauceRestApi();
        } catch (TestManagerException e) {
            LOG.info("SauceLabs credentials are missing");
            haveSaucelabs = false;
        }

        if (sauceApi != null) {
            if (status.equals("failed")) {
                sauceApi.jobFailed(TestManager.getInstance().getDriverSessionId());
            } else if (status.equals("passed")) {
                sauceApi.jobPassed(TestManager.getInstance().getDriverSessionId());
            }
        }
    }

    private String getJobLink() {
        String jobLink = "";

        // Get JobLink
        try {
            jobLink = TestManager.getSauceLabsJobLink();
        } catch (TestManagerException e) {
            LOG.info("SauceLabs credentials are missing");
        }
        return jobLink;
    }

    private void addHooks(Match match, Result result) {
        List<Map> hooks = getFeatureElement().get("after");
        if (hooks == null) {
            hooks = new ArrayList<>();
            getFeatureElement().put("after", hooks);
        }
        hooks.add(buildHookMap(match, result));
    }

    private Map getCurrentStep(Phase phase) {
        String target = phase.ordinal() <= Phase.match.ordinal() ? Phase.match.name() : Phase.result.name();
        Map lastWithValue = null;
        for (Map stepOrHook : getSteps()) {
            if (stepOrHook.get(target) == null) {
                return stepOrHook;
            } else {
                lastWithValue = stepOrHook;
            }
        }
        return lastWithValue;
    }

    private Map buildHookMap(final Match match, final Result result) {
        final Map hookMap = new HashMap();
        hookMap.put("match", match.toMap());
        hookMap.put("result", result.toMap());
        return hookMap;
    }

    @Override
    public void match(Match match) {
        getCurrentStep(Phase.match).put("match", match.toMap());
    }

    @Override
    public void embedding(String mimeType, byte[] data) {
        final Map<String, String> embedding = new HashMap<String, String>();
        embedding.put("mime_type", mimeType);
        embedding.put("data", Base64.encodeBytes(data));
        getEmbeddings().add(embedding);
    }

    @Override
    public void write(String text) {
        getOutput().add(text);
    }

    private List<Map<String, Object>> getFeatureElements() {
        List<Map<String, Object>> featureElements = (List) featureMap.get("elements");
        if (featureElements == null) {
            featureElements = new ArrayList<>();
            featureMap.put("elements", featureElements);
        }
        return featureElements;
    }

    private Map<Object, List<Map>> getFeatureElement() {
        if (getFeatureElements().size() > 0) {
            return (Map) getFeatureElements().get(getFeatureElements().size() - 1);
        } else {
            return null;
        }
    }

    private List<Map> getAllExamples() {
        List<Map> allExamples = getFeatureElement().get("examples");
        if (allExamples == null) {
            allExamples = new ArrayList<>();
            getFeatureElement().put("examples", allExamples);
        }
        return allExamples;
    }

    private List<Map> getSteps() {
        List<Map> steps = getFeatureElement().get("steps");
        if (steps == null) {
            steps = new ArrayList<>();
            getFeatureElement().put("steps", steps);
        }
        return steps;
    }

    private List<Map<String, String>> getEmbeddings() {
        List<Map<String, String>> embeddings = (List<Map<String, String>>) getCurrentStep(Phase.embedding).get("embeddings");
        if (embeddings == null) {
            embeddings = new ArrayList<>();
            getCurrentStep(Phase.embedding).put("embeddings", embeddings);
        }
        return embeddings;
    }

    private List<String> getOutput() {
        List<String> output = (List<String>) getCurrentStep(Phase.output).get("output");
        if (output == null) {
            output = new ArrayList<>();
            getCurrentStep(Phase.output).put("output", output);
        }
        return output;
    }
}
