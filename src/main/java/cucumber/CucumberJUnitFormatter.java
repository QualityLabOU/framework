package cucumber;

import cucumber.runtime.formatter.StrictAware;
import gherkin.formatter.Formatter;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import utils.TestManager;
import utils.TestManagerException;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CucumberJUnitFormatter implements Formatter, Reporter, StrictAware {

    private TestCase testCase;
    private Element root;

    public CucumberJUnitFormatter(URL out) throws IOException {
        if (!CucumberAutomationTest.junitFormatterActive) {
            synchronized (CucumberAutomationTest.class) {
                CucumberAutomationTest.junitFormatterActive = true;
                CucumberAutomationTest.initJUnit(out);
            }
        }
    }

    @Override
    public void feature(Feature feature) {
        TestCase.feature = feature;
        TestCase.previousScenarioOutlineName = "";
        TestCase.exampleNumber = 1;
    }

    @Override
    public void background(Background background) {
        if (!isCurrentTestCaseCreatedNameless()) {
            testCase = new TestCase();
            root = testCase.createElement(CucumberAutomationTest.doc);
        }
    }

    @Override
    public void scenario(Scenario scenario) {
        if (isCurrentTestCaseCreatedNameless()) {
            testCase.scenario = scenario;
        } else {
            testCase = new TestCase(scenario);
            root = testCase.createElement(CucumberAutomationTest.doc);
        }
        testCase.writeElement(CucumberAutomationTest.doc, root);
        CucumberAutomationTest.rootElement.appendChild(root);

        increaseAttributeValue(CucumberAutomationTest.rootElement, "tests");
    }

    private boolean isCurrentTestCaseCreatedNameless() {
        return testCase != null && testCase.scenario == null;
    }

    @Override
    public void step(Step step) {
        if (testCase != null) testCase.steps.add(step);
    }

    @Override
    public void done() {

    }

    @Override
    public void startOfScenarioLifeCycle(Scenario scenario) {
        // NoOp
    }

    @Override
    public void endOfScenarioLifeCycle(Scenario scenario) {
        if (testCase != null && testCase.steps.isEmpty()) {
            testCase.handleEmptyTestCase(CucumberAutomationTest.doc, root);
        }
    }

    @Override
    public void result(Result result) {
        testCase.results.add(result);
        testCase.updateElement(CucumberAutomationTest.doc, root);
    }

    @Override
    public void before(Match match, Result result) {
        if (!isCurrentTestCaseCreatedNameless()) {
            testCase = new TestCase();
            root = testCase.createElement(CucumberAutomationTest.doc);
        }
        handleHook(result);
    }

    @Override
    public void after(Match match, Result result) {
        handleHook(result);
    }

    private void handleHook(Result result) {
        testCase.hookResults.add(result);
        testCase.updateElement(CucumberAutomationTest.doc, root);
    }

    private void increaseAttributeValue(Element element, String attribute) {
        int value = 0;
        if (element.hasAttribute(attribute)) {
            value = Integer.parseInt(element.getAttribute(attribute));
        }
        element.setAttribute(attribute, String.valueOf(++value));
    }

    @Override
    public void scenarioOutline(ScenarioOutline scenarioOutline) {
        testCase = null;
    }

    @Override
    public void examples(Examples examples) {
    }

    @Override
    public void match(Match match) {
    }

    @Override
    public void embedding(String mimeType, byte[] data) {
    }

    @Override
    public void write(String text) {
    }

    @Override
    public void uri(String uri) {
    }

    @Override
    public void close() {
    }

    @Override
    public void eof() {
    }

    @Override
    public void syntaxError(String state, String event, List<String> legalEvents, String uri, Integer line) {
    }

    @Override
    public void setStrict(boolean strict) {
        TestCase.treatSkippedAsFailure = strict;
    }

    public static class TestCase {
        private static final DecimalFormat NUMBER_FORMAT = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);

        static {
            NUMBER_FORMAT.applyPattern("0.######");
        }

        public TestCase(Scenario scenario) {
            this.scenario = scenario;
        }

        public TestCase() {
        }

        Scenario scenario;
        static Feature feature;
        static String previousScenarioOutlineName;
        static int exampleNumber;
        static boolean treatSkippedAsFailure = false;
        final List<Step> steps = new ArrayList<Step>();
        final List<Result> results = new ArrayList<Result>();
        final List<Result> hookResults = new ArrayList<Result>();

        private Element createElement(Document doc) {
            return doc.createElement("testcase");
        }

        private void writeElement(Document doc, Element tc) {
            String deviceName = " [" + TestManager.getInstance().getCurrentDevice().getName() + "]";
            tc.setAttribute("classname", feature.getName() + deviceName);

            tc.setAttribute("name", calculateElementName(scenario, deviceName));
        }

        private String calculateElementName(Scenario scenario, String deviceName) {
            String scenarioName = scenario.getName() + deviceName;
            if (scenario.getKeyword().equals("Scenario Outline") && scenarioName.equals(previousScenarioOutlineName)) {
                return scenarioName + (includesBlank(scenarioName) ? " " : "_") + ++exampleNumber;
            } else {
                previousScenarioOutlineName = scenario.getKeyword().equals("Scenario Outline") ? scenarioName : "";
                exampleNumber = 1;
                return scenarioName;
            }
        }

        private boolean includesBlank(String scenarioName) {
            return scenarioName.indexOf(' ') != -1;
        }

        public void updateElement(Document doc, Element tc) {
            tc.setAttribute("time", calculateTotalDurationString());

            StringBuilder sb = new StringBuilder();
            addStepAndResultListing(sb);
            Result skipped = null, failed = null;
            for (Result result : results) {
                if ("failed".equals(result.getStatus())) failed = result;
                if ("undefined".equals(result.getStatus()) || "pending".equals(result.getStatus())) skipped = result;
            }
            for (Result result : hookResults) {
                if (failed == null && "failed".equals(result.getStatus())) failed = result;
            }
            Element child;
            if (failed != null) {
                addStackTrace(sb, failed);
                String errorMessage = failed.getErrorMessage() + System.getProperty("line.separator") + getJobLink();
                child = createElementWithMessage(doc, sb, "failure", errorMessage);
            } else if (skipped != null) {
                if (treatSkippedAsFailure) {
                    child = createElementWithMessage(doc, sb, "failure", "The scenario has pending or undefined step(s)");
                } else {
                    child = createElement(doc, sb, "skipped");
                }
            } else {
                child = createElement(doc, sb, "system-out");
            }

            Node existingChild = tc.getFirstChild();
            if (existingChild == null) {
                tc.appendChild(child);
            } else {
                tc.replaceChild(child, existingChild);
            }
        }

        public void handleEmptyTestCase(Document doc, Element tc) {
            tc.setAttribute("time", calculateTotalDurationString());

            String resultType = treatSkippedAsFailure ? "failure" : "skipped";
            Element child = createElementWithMessage(doc, new StringBuilder(), resultType, "The scenario has no steps");

            tc.appendChild(child);
        }

        private String calculateTotalDurationString() {
            long totalDurationNanos = 0;
            for (Result r : results) {
                totalDurationNanos += r.getDuration() == null ? 0 : r.getDuration();
            }
            for (Result r : hookResults) {
                totalDurationNanos += r.getDuration() == null ? 0 : r.getDuration();
            }
            double totalDurationSeconds = ((double) totalDurationNanos) / 1000000000;
            return NUMBER_FORMAT.format(totalDurationSeconds);
        }

        private void addStepAndResultListing(StringBuilder sb) {
            for (int i = 0; i < steps.size(); i++) {
                int length = sb.length();
                String resultStatus = "not executed";
                if (i < results.size()) {
                    resultStatus = results.get(i).getStatus();
                }
                sb.append(steps.get(i).getKeyword());
                sb.append(steps.get(i).getName());
                do {
                    sb.append(".");
                } while (sb.length() - length < 76);
                sb.append(resultStatus);
                sb.append("\n");
            }
        }

        private void addStackTrace(StringBuilder sb, Result failed) {
            sb.append("\nStackTrace:\n");
            StringWriter sw = new StringWriter();
            failed.getError().printStackTrace(new PrintWriter(sw));
            sb.append(sw.toString());
        }

        private Element createElementWithMessage(Document doc, StringBuilder sb, String elementType, String message) {
            Element child = createElement(doc, sb, elementType);
            child.setAttribute("message", message);
            return child;
        }

        private Element createElement(Document doc, StringBuilder sb, String elementType) {
            Element child = doc.createElement(elementType);
            child.appendChild(doc.createCDATASection(sb.toString()));
            return child;
        }
    }

    private static String getJobLink() {
        String jobLink = "";

        // Get JobLink
        try {
            jobLink = TestManager.getSauceLabsJobLink();
        } catch (TestManagerException ignored) {
        }
        return jobLink;
    }
}
