package cucumber;

import at.AutomationTest;
import cucumber.runtime.CucumberException;
import cucumber.runtime.io.URLOutputStream;
import cucumber.runtime.io.UTF8OutputStreamWriter;
import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.GsonBuilder;
import gherkin.formatter.NiceAppendable;
import models.Device;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CucumberAutomationTest extends AutomationTest {

    private static final Logger LOG = LogManager.getLogger(CucumberAutomationTest.class);

    private static List<Map<String, Object>> features;
    public static NiceAppendable out;

    public static Writer writer;
    public static Document doc;
    public static Element rootElement;

    public static boolean junitFormatterActive = false;
    public static boolean junitInitialized = false;

    public CucumberAutomationTest(Device device) {
        super(device);
    }

    @BeforeSuite
    public void executeBeforeSuite() {
        initFeatures();
    }

    @AfterSuite
    public void executeAfterSuite() {
        try {
            generateReport();

            if (junitFormatterActive) {
                generateJUnitFormatterReport(doc, writer);
            }
        } catch (Exception e) {
            LOG.error("FAILED ON EXECUTE AFTER WITH MESSAGE: " + e.getMessage());
        }
    }

    public static List<Map<String, Object>> getFeatures() {
        LOG.info("GETTING FEATURES");
        return features;
    }

    public static void initJUnit(URL urlOut) {
        if (!junitInitialized) {
            try {
                writer = new UTF8OutputStreamWriter(new URLOutputStream(urlOut));
                doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
                rootElement = CucumberAutomationTest.doc.createElement("testsuite");
                doc.appendChild(CucumberAutomationTest.rootElement);
                CucumberJUnitFormatter.TestCase.treatSkippedAsFailure = false;
                junitInitialized = true;
            } catch (ParserConfigurationException e) {
                throw new CucumberException("Error while processing unit report", e);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void generateHtmlReport(String name, String buildNr, String outputDir, List<String> jsonFilePaths) {
        File reportOutputDirectory = new File(outputDir);

        if (!reportOutputDirectory.exists()) {
            LOG.info("Cucumber HTML report directory do not exist, creating one");
            reportOutputDirectory.mkdirs();
            LOG.info("Cucumber HTML report directory created");
        }

        List<String> jsonFiles = new ArrayList<>();
        jsonFiles.addAll(jsonFilePaths);

        boolean skippedFails = true;
        boolean pendingFails = false;
        boolean undefinedFails = false;
        boolean missingFails = false;

        Configuration configuration = new Configuration(reportOutputDirectory, name);
        configuration.setStatusFlags(skippedFails, pendingFails, undefinedFails, missingFails);
        configuration.setParallelTesting(false);
        configuration.setJenkinsBasePath("");
        configuration.setRunWithJenkins(false);
        configuration.setBuildNumber(buildNr);

        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        reportBuilder.generateReports();
        LOG.info("Cucumber HTML report generated");
    }

    private void initFeatures() {
        features = new ArrayList<>();
    }

    private void generateJUnitFormatterReport(Document doc, Writer out) {
        // set up a transformer
        rootElement.setAttribute("name", CucumberJUnitFormatter.class.getName());
        rootElement.setAttribute("failures", String.valueOf(rootElement.getElementsByTagName("failure").getLength()));
        rootElement.setAttribute("skipped", String.valueOf(rootElement.getElementsByTagName("skipped").getLength()));
        rootElement.setAttribute("time", sumTimes(rootElement.getElementsByTagName("testcase")));

        try {
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            StreamResult result = new StreamResult(out);
            DOMSource source = new DOMSource(doc);
            trans.transform(source, result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    private String sumTimes(NodeList testCaseNodes) {
        double totalDurationSecondsForAllTimes = 0.0d;
        for (int i = 0; i < testCaseNodes.getLength(); i++) {
            try {
                double testCaseTime =
                        Double.parseDouble(testCaseNodes.item(i).getAttributes().getNamedItem("time").getNodeValue());
                totalDurationSecondsForAllTimes += testCaseTime;
            } catch (NumberFormatException | NullPointerException e) {
                throw new CucumberException(e);
            }
        }
        DecimalFormat nfmt = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);
        nfmt.applyPattern("0.######");
        return nfmt.format(totalDurationSecondsForAllTimes);
    }

    private void generateReport() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
        out.append("[");
        boolean first = true;
        for (Map<String, Object> feature : getFeatures()) {
            if (!first) out.append(",");
            out.append(gson.toJson(feature));
            first = false;
        }
        out.append("]");
        out.close();
    }
}
