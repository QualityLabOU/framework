package at;

import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.common.SauceOnDemandSessionIdProvider;
import com.saucelabs.testng.SauceOnDemandAuthenticationProvider;
import com.saucelabs.testng.SauceOnDemandTestListener;
import models.Device;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.TestNG;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import static utils.TestManager.getInstance;

public class AutomationTest implements SauceOnDemandAuthenticationProvider, SauceOnDemandSessionIdProvider {

    private static final Logger LOG = LogManager.getLogger(AutomationTest.class);
    private Device device;
    private SauceOnDemandAuthentication authenticationSauceLabs = new SauceOnDemandAuthentication();

    protected Device getDevice() {
        return device;
    }

    @DataProvider(name = "dataProvider", parallel = true)
    public static Object[][] getDataProvider() {
        return getInstance().getDataProvider();
    }

    public AutomationTest(Device device) {
        this.device = device;

        TestNG testng = new TestNG();
        String[] testFarmCredentials = getInstance().getTestFarmCredentials();

        if (testFarmCredentials[0].equals("SAUCELABS")) {

            testng.addListener(new SauceOnDemandTestListener());
            authenticationSauceLabs = new SauceOnDemandAuthentication(testFarmCredentials[1], testFarmCredentials[2]);
        }
    }

    @BeforeMethod
    public void executeBeforeMethod(ITestContext context) {
        try {
            getInstance().setCurrentDevice(getDevice());
        } catch (Exception e) {
            LOG.error("SEVERE: Failed to set driver.", e);
        }
    }

    @Override
    public SauceOnDemandAuthentication getAuthentication() {
        return authenticationSauceLabs;
    }

    @Override
    public String getSessionId() {
        return getInstance().getDriverSessionId();
    }
}
