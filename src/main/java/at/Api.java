package at;

import interfaces.IPageObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.TestException;
import utils.TestManager;

import java.util.ArrayList;
import java.util.List;

public class Api {

    private static final Logger LOG = LogManager.getLogger(Api.class);

    private static final long DEFAULT_WAIT_TIMEOUT = 30;

    public static WebElement $(By by, long timeOut) {
        return waitForElementPresence(by, timeOut);
    }

    public static WebElement $(By by) {
        return $(by, DEFAULT_WAIT_TIMEOUT);
    }

    public static List<WebElement> $$(By by, long timeOut) {
        return waitForElementsPresence(by, timeOut);
    }

    public static List<WebElement> $$(By by) {
        return $$(by, DEFAULT_WAIT_TIMEOUT);
    }

    public static void open() {
        TestManager.getInstance().open();
    }

    public static void open(String url) {
        TestManager.getInstance().open(url);
    }

    public static boolean isDisplayed(By by) {
        try {
            WebElement element = $(by);
            return element != null && element.isDisplayed();
        } catch (TimeoutException e) {
            LOG.info("Element with locator " + by + " is not displayed");
            return false;
        } catch (WebDriverException e) {
            return false;
        }
    }

    public static boolean isPresent(IPageObject pageObject) {

        return pageObject.isPresent();
    }

    public static void click(WebElement element) {
        waitForElementClickable(element, DEFAULT_WAIT_TIMEOUT);
        element.click();
    }

    public static void scrollIntoViewAndClick(WebElement element) {
        waitForElementClickable(element, DEFAULT_WAIT_TIMEOUT);
        scrollIntoView(element);
        element.click();
    }

    public static void fillIn(WebElement element, String text) {
        element.sendKeys(text);
    }

    public static void clearAndfillIn(WebElement element, String text) {
        element.clear();
        fillIn(element, text);
    }

    public static void scrollIntoView(WebElement element) {
        ((JavascriptExecutor) TestManager.getInstance().getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public static void scrollTo(int x, int y) {
        ((JavascriptExecutor) TestManager.getInstance().getDriver()).executeScript("scrollTo(" + x + "," + y + ")");
    }

    public static WebElement waitForElement(ExpectedCondition<WebElement> condition, long timeOut) {
        WebDriverWait w = new WebDriverWait(TestManager.getInstance().getDriver(), timeOut);
        try {
            return w.until(condition);
        } catch (TimeoutException e) {
            LOG.error("Timeout after waiting for element (WITH CONDITION: " + condition + ") for " + timeOut + " seconds.");
            throw new TimeoutException(e.getMessage());
        }
    }

    public static WebElement waitForElement(ExpectedCondition<WebElement> condition) {
        return waitForElement(condition, DEFAULT_WAIT_TIMEOUT);
    }

    public static List<WebElement> waitForElements(ExpectedCondition<List<WebElement>> condition, long timeOut) {
        WebDriverWait w = new WebDriverWait(TestManager.getInstance().getDriver(), timeOut);
        return w.until(condition);
    }

    public static List<WebElement> waitForElements(ExpectedCondition<List<WebElement>> condition) {
        return waitForElements(condition, DEFAULT_WAIT_TIMEOUT);
    }

    public static WebElement waitForElementPresence(By locator, long timeOut) {
        try {
            return waitForElement(ExpectedConditions.presenceOfElementLocated(locator), timeOut);
        } catch (TimeoutException e) {
            throw new NoSuchElementException("Failed to find element with locator: " + locator);
        }
    }

    public static List<WebElement> waitForElementsPresence(By locator, long timeOut) {
        try {
            return waitForElements(ExpectedConditions.presenceOfAllElementsLocatedBy(locator), timeOut);
        } catch (TimeoutException e) {
            throw new NoSuchElementException("Failed to find element with locator: " + locator);
        }
    }

    public static WebElement waitForElementClickable(WebElement element, long timeOut) {
        try {
            return waitForElement(ExpectedConditions.elementToBeClickable(element), timeOut);
        } catch (TimeoutException e) {
            throw new TestException("Element is not clickable!" + "Element: " + element);
        }
    }

    public static boolean waitForCondition(ExpectedCondition<Boolean> condition, long timeOut) {
        try {
            WebDriverWait w = new WebDriverWait(TestManager.getInstance().getDriver(), timeOut);
            return w.until(condition);
        } catch (TimeoutException e) {
            throw new TestException("Waiting for condition failed! " + "CONDITION: " + condition);
        }
    }

    public static boolean waitForCondition(ExpectedCondition<Boolean> condition, String message, long timeOut) {
        try {
            WebDriverWait w = new WebDriverWait(TestManager.getInstance().getDriver(), timeOut);
            return w.until(condition);
        } catch (TimeoutException e) {
            throw new TestException("Waiting for condition failed! " + "CONDITION: " + message);
        }
    }

    public static boolean waitForCondition(ExpectedCondition<Boolean> condition) {
        return waitForCondition(condition, DEFAULT_WAIT_TIMEOUT);
    }

    public static boolean waitForCondition(ExpectedCondition<Boolean> condition, String message) {
        return waitForCondition(condition, message, DEFAULT_WAIT_TIMEOUT);
    }

    public static boolean waitForAngular(long timeOut) {
        WebDriverWait w = new WebDriverWait(TestManager.getInstance().getDriver(), timeOut);
        boolean hasAngular = w.until((ExpectedCondition<Boolean>) driver ->
                ((JavascriptExecutor) driver).executeScript("return typeof angular").toString().equals("object"));
        return hasAngular;
    }

    public static boolean waitForJQuery() {

        WebDriverWait wait = new WebDriverWait(TestManager.getInstance().getDriver(), 30);
        boolean isJSLoaded = wait.until((ExpectedCondition<Boolean>) driver ->
                (Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
        return isJSLoaded;
    }

    public static List<String> getBoundedRectangleOfElement(WebElement we) {
        JavascriptExecutor je = (JavascriptExecutor) TestManager.getInstance().getDriver();
        List<String> bounds = (ArrayList<String>) je.executeScript(
                "var rect = arguments[0].getBoundingClientRect();" +
                        "return [ '' + parseInt(rect.left), '' + parseInt(rect.top), '' + parseInt(rect.width), '' + parseInt(rect.height) ]", we);
        return bounds;
    }

    public static boolean waitForAngular() {
        return waitForAngular(DEFAULT_WAIT_TIMEOUT);
    }

    public static String getCurrentUrl() {
        return TestManager.getInstance().getDriver().getCurrentUrl();
    }

    public static String getTitle() {
        return TestManager.getInstance().getDriver().getTitle();
    }

    public static WebDriver.Navigation getNavigation() {
        return TestManager.getInstance().getDriver().navigate();
    }

    public static void back() {
        getNavigation().back();
    }

    public static void forward() {
        getNavigation().forward();
    }

    public static void refresh() {
        getNavigation().refresh();
    }

    public static int getX(WebElement element) {
        return element.getLocation().getX();
    }

    public static int getY(WebElement element) {
        return element.getLocation().getY();
    }

    public static int getWidth(WebElement element) {
        return element.getSize().getWidth();
    }

    public static int getHeight(WebElement element) {
        return element.getSize().getHeight();
    }

    public static String getConfigURL() {
        return TestManager.getInstance().getConfig().getUrl();
    }

}
