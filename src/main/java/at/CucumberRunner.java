package at;

import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.testng.TestNGCucumberRunner;
import cucumber.runtime.model.CucumberFeature;
import gherkin.formatter.model.Comment;
import gherkin.formatter.model.Tag;
import models.Feature;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CucumberOptions(
        features = "src/test/resources/features",
        tags = {"~@TODO"}
)
public class CucumberRunner {

    TestNGCucumberRunner runner;

    public CucumberRunner() {
        runner = new TestNGCucumberRunner(getClass());
    }

    public List<Feature> getAvailableFeatures() {
        List<CucumberFeature> cucumberFeatures = runner.getFeatures();
        List<Feature> features = new ArrayList<>();
        for (CucumberFeature cucumberFeature : cucumberFeatures) {
            String path = cucumberFeature.getPath();
            List<String> tags = cucumberFeature.getGherkinFeature().getTags().stream().map(Tag::getName).collect(Collectors.toList());
            String name = cucumberFeature.getGherkinFeature().getName();
            List<String> comments = cucumberFeature.getGherkinFeature().getComments().stream().map(Comment::getValue).collect(Collectors.toList());
            features.add(new Feature(path, name, comments, tags, new ArrayList<Scenario>()));
        }
        return features;
    }

}
