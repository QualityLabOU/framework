package models;

import java.util.List;

public class Scenario {

    private String name;
    private List<String> tags;
    private List<String> steps;

    public Scenario(String name, List<String> tags, List<String> steps) {
        this.name = name;
        this.tags = tags;
        this.steps = steps;
    }

    public String getName() {
        return name;
    }

    public List<String> getTags() {
        return tags;
    }

    public List<String> getSteps() {
        return steps;
    }
}
