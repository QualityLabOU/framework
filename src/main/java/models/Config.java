package models;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class represents test configurations
 */
public class Config {

    private static final Logger LOG = LogManager.getLogger(Config.class);

    private enum ConfigType {
        WEB,
        MWEB
    }

    private enum TestFarmProvider {
        NONE,
        SAUCELABS
    }

    private String name;
    private String versionUrl;
    private String version;

    private ConfigType type;
    private String url;

    private TestFarmProvider testFarmProvider;
    private String testFarmUser;
    private String testFarmPassword;
    private int devicesPerRun;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersionUrl() {
        return versionUrl;
    }

    public void setVersionUrl(String versionUrl) {
        this.versionUrl = versionUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private ConfigType getType() {
        return type != null ? type : ConfigType.WEB;
    }

    public String getTypeName() {
        return getType().name();
    }

    public void setType(String type) {
        try {
            this.type = ConfigType.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException e) {
            LOG.info("Type is not present on config");
        }

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private TestFarmProvider getTestFarmProvider() {
        return testFarmProvider != null ? testFarmProvider : TestFarmProvider.NONE;
    }

    public String getTestFarmProviderName() {
        return getTestFarmProvider().name();
    }


    public void setTestFarmProvider(String testFarmProvider) {
        try {
            this.testFarmProvider = TestFarmProvider.valueOf(testFarmProvider.toUpperCase());
        } catch (IllegalArgumentException e) {
            this.testFarmProvider = TestFarmProvider.NONE;
            LOG.info("No TestFarm provider");
        }
    }

    public String getTestFarmUser() {
        return testFarmUser;
    }

    public void setTestFarmUser(String testFarmUser) {
        this.testFarmUser = testFarmUser;
    }

    public String getTestFarmPassword() {
        return testFarmPassword;
    }

    public void setTestFarmPassword(String testFarmPassword) {
        this.testFarmPassword = testFarmPassword;
    }

    public int getDevicesPerRun() {
        return devicesPerRun;
    }

    public void setDevicesPerRun(int devicesPerRun) {
        this.devicesPerRun = devicesPerRun;
    }

    public void setDevicesPerRun(String devicesPerRun) {
        try {
            setDevicesPerRun(Integer.parseInt(devicesPerRun));
        } catch (NumberFormatException e) {
            LOG.error("Cannot parse devicesPerRun tag.");
        }
    }

    public boolean hasName() {
        return StringUtils.isNotBlank(this.name);
    }

    public boolean hasUrl() {
        return StringUtils.isNotBlank(this.url);
    }

    private boolean hasTestFarmProvider(TestFarmProvider testFarmProvider) {
        return getTestFarmProvider().equals(testFarmProvider);
    }

    public boolean hasTestFarmProvider() {
        return !hasTestFarmProvider(TestFarmProvider.NONE);
    }

    public boolean hasSauceLabsTestFarmProvider() {
        return hasTestFarmProvider(TestFarmProvider.SAUCELABS);
    }

    public boolean isValid() {
        return hasUrl();
    }

    public boolean isWeb() {
        return getType().equals(ConfigType.WEB);
    }

    public boolean isMWeb() {
        return getType().equals(ConfigType.MWEB);
    }


    public boolean shouldRandomizeDevices(int totalDevices) {
        return (getDevicesPerRun() > 0 && getDevicesPerRun() < totalDevices);
    }

    @Override
    public String toString() {
        return "name:" + name
                + " type:" + type
                + " url:" + url
                + " testFarmProvider:" + testFarmProvider
                + " devicesPerRun:" + devicesPerRun;
    }

}
