package models;


import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class represents test devices
 */
public class Device {

    private static final Logger LOG = LogManager.getLogger(Device.class);

    private enum Orientation {
        PORTRAIT,
        LANDSCAPE
    }

    private enum Platform {

        NONE,
        WINDOWS,
        OSX,
        LINUX,
        ANDROID,
        IOS
    }

    private enum Browser {

        NONE,
        BROWSER,
        CHROME,
        FIREFOX,
        IE,
        SAFARI
    }

    private String name;
    private Platform platform;
    private String platformVersion;
    private Browser browser;
    private String browserVersion;
    private String id;
    private String deviceType;
    private Orientation deviceOrientation;

    // Optional parameters:
    private Boolean recordVideo;
    private Boolean recordScreenshots;
    private Boolean recordLogs;
    private Boolean captureHtml;
    private Boolean acceptCertificate;
    private String tunnelIdentifier;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private Platform getPlatform() {
        return platform != null ? platform : Platform.NONE;
    }

    public String getPlatformName() {
        return getPlatform().name();
    }

    public void setPlatform(String platform) {

        try {
            this.platform = Platform.valueOf(platform.toUpperCase());
        } catch (IllegalArgumentException e) {
            LOG.info("Cannot parse platform");
        }
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    private Browser getBrowser() {
        return browser != null ? browser : Browser.NONE;
    }

    public String getBrowserName() {
        return getBrowser().name();
    }

    public void setBrowser(String browser) {
        try {
            this.browser = Browser.valueOf(browser.toUpperCase());
        } catch (IllegalArgumentException e) {
            LOG.info("Cannot parse browser");
        }
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceType() {
        return deviceType != null ? deviceType.toLowerCase() : "";
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceOrientation() {
        if (deviceOrientation == null) return Orientation.PORTRAIT.toString().toLowerCase();
        return deviceOrientation.toString().toLowerCase();
    }

    public void setDeviceOrientation(String deviceOrientation) {
        try {
            this.deviceOrientation = Orientation.valueOf(deviceOrientation.toUpperCase());
        }catch (IllegalArgumentException e) {
            this.deviceOrientation = Orientation.PORTRAIT;
            LOG.info("Set device orientation to portrait");
        }
    }

    public boolean hasName() {
        return StringUtils.isNotBlank(this.name);
    }

    private boolean hasPlatform(Platform platform) {
        return getPlatform().equals(platform);
    }

    public boolean hasPlatform() {
        return !hasPlatform(Platform.NONE);
    }

    public boolean hasWindowsPlatform() {
        return hasPlatform(Platform.WINDOWS);
    }

    public boolean hasOSXPlatform() {
        return hasPlatform(Platform.OSX);
    }

    public boolean hasLinuxPlatform() {
        return hasPlatform(Platform.LINUX);
    }

    public boolean hasAndroidPlatform() {
        return hasPlatform(Platform.ANDROID);
    }

    public boolean hasIOSPlatform() {
        return hasPlatform(Platform.IOS);
    }



    private boolean hasBrowser(Browser browser) {
        return getBrowser().equals(browser);
    }

    public boolean hasBrowser() {
        return !hasBrowser(Browser.NONE);
    }

    public boolean hasBrowserBrowser() {
        return hasBrowser(Browser.BROWSER);
    }

    public boolean hasChromeBrowser() {
        return hasBrowser(Browser.CHROME);
    }

    public boolean hasFirefoxBrowser() {
        return hasBrowser(Browser.FIREFOX);
    }

    public boolean hasIEBrowser() {
        return hasBrowser(Browser.IE);
    }

    public boolean hasSafariBrowser() {
        return hasBrowser(Browser.SAFARI);
    }


    public Boolean getRecordScreenshots() {
        return recordScreenshots;
    }

    public void setRecordScreenshots(String recordscreenshots) {
        if (recordscreenshots.equals("false")) {
            this.recordScreenshots = false;
        } else if (recordscreenshots.equals("true")) {
            this.recordScreenshots = true;
        }
    }

    public Boolean getRecordVideo() {
        return recordVideo;
    }

    public void setRecordVideo(String recordvideo) {
        if (recordvideo.equals("false")) {
            this.recordVideo = false;
        } else if (recordvideo.equals("true")) {
            this.recordVideo = true;
        }
    }

    public Boolean getAcceptCertificate() {
        return acceptCertificate;
    }

    public void setAcceptCertificate(String certificateState) {
        if (certificateState.equals("false")) {
            this.acceptCertificate = false;
        } else if (certificateState.equals("true")) {
            this.acceptCertificate = true;
        }
    }


    public String getTunnelIdentifier() {
        return tunnelIdentifier;
    }

    public void setTunnelIdentifier(String tunnelIdentifier) {
        this.tunnelIdentifier = tunnelIdentifier;
    }

    public Boolean getRecordLogs() {
        return recordLogs;
    }

    public void setRecordLogs(String recordlogs) {
        if (recordlogs.equals("false")) {
            this.recordLogs = false;
        } else if (recordlogs.equals("true")) {
            this.recordLogs = true;
        }
    }

    public Boolean getCaputeHtml() {
        return captureHtml;
    }

    public void setCaputeHtml(String capturehtml) {
        if (capturehtml.equals("false")) {
            this.captureHtml = false;
        } else if (capturehtml.equals("true")) {
            this.captureHtml = true;
        }
    }


    @Override
    public String toString() {
        return "name:" + name
                + " platform:" + platform
                + " platformVersion:" + platformVersion
                + " browser:" + browser
                + " browserVersion:" + browserVersion
                + " id:" + id
                + " recordVideo:" + recordVideo;
    }
}
