package models;

import java.util.List;

public class Feature {

    private String path;
    private String name;
    private List<String> comments;
    private List<String> tags;
    private List<cucumber.api.Scenario> scenarios;

    public Feature(String path, String name, List<String> comments, List<String> tags, List<cucumber.api.Scenario> scenarios) {
        this.path = path;
        this.name = name;
        this.comments = comments;
        this.tags = tags;
        this.scenarios = scenarios;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public List<String> getComments() {
        return comments;
    }

    public List<String> getTags() {
        return tags;
    }

    public List<cucumber.api.Scenario> getScenarios() {
        return scenarios;
    }
}
