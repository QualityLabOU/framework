package utils;

import com.saucelabs.saucerest.SauceREST;
import models.Config;
import models.Device;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.TestException;

import java.util.*;

public class TestManager {

    private static final Logger LOG = LogManager.getLogger(TestManager.class);

    private static final String SYSTEM_PROPERTY_CONFIG_NAME = "config-name";

    private Config config = new Config();
    private List<Device> devices = new ArrayList<>();

    private static UUID runId;

    private static ThreadLocal<WebDriver> drivers = new ThreadLocal<>();
    private static ThreadLocal<String> driverSessionIds = new ThreadLocal<>();
    private static ThreadLocal<Device> currentDevices = new ThreadLocal<>();

    private static TestManager instance;

    public TestManager() {
        try {
            setRunId();
            setValidConfig(XmlManager.parseConfigs());
            setValidDevices(XmlManager.parseDevices());

        } catch (TestManagerException e) {
            LOG.error(e.getMessage(), e);
        } catch (Exception e) {
            LOG.error("SEVERE: failed to parse one of the XML files.", e);
        }
    }

    public static TestManager getInstance() {
        if (instance == null) {
            synchronized (TestManager.class) {

                if (instance == null)
                    instance = new TestManager();
            }
        }
        return instance;
    }

    public Config getConfig() {
        return config;
    }

    private void setConfig(Config config) {
        this.config = config;
    }

    private List<Device> getDevices() {
        return devices;
    }

    private void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    private void setRunId() {
        runId = UUID.randomUUID();
    }

    public static UUID getRunId() {
        return runId;
    }

    protected void setValidConfig(List<Config> configs) throws TestManagerException {

        String configName = System.getProperty(SYSTEM_PROPERTY_CONFIG_NAME, Configuration.DEFAULT_CONFIG_NAME);
        for (Config c : configs) {
            if (c.isValid() && c.getName().equals(configName)) {

                LOG.info("VALID CONFIG: " + c.getName());
                setConfig(c);
                break;
            }
        }

        if (getConfig().getName() == null) throw new TestManagerException("No valid config found.");
    }


    protected void setValidDevices(List<Device> devices) throws TestManagerException {
        List<Device> validDevices = new ArrayList<>();

        if (getConfig() != null) {
            validDevices = findValidDevices(devices, getConfig());

            //randomize devices if needed
            if (getConfig().shouldRandomizeDevices(validDevices.size())) {
                validDevices = randomizeDevices(validDevices, getConfig().getDevicesPerRun());
            }
        }
        LOG.info("VALID DEVICES: " + validDevices.size());
        setDevices(validDevices);

        if (getDevices().isEmpty()) throw new TestManagerException("No valid devices found.");
    }

    private List<Device> findValidDevices(List<Device> devices, Config config) {

        List<Device> validDevices = new ArrayList<>();

        for (Device device : devices) {

            if (config.isWeb() && device.hasBrowser()) validDevices.add(device);
            if (config.isMWeb() && device.hasPlatform() && device.hasBrowser())
                validDevices.add(device);
        }
        return validDevices;
    }

    private List<Device> randomizeDevices(List<Device> devices, int count) {

        List<Device> copy = new LinkedList<>(devices);
        Collections.shuffle(copy);
        return copy.subList(0, count);
    }

    public Object[][] getDataProvider() {
        List devices = getDevices();
        Object[][] dataProvider = new Object[devices.size()][1];

        for (int i = 0; i < devices.size(); ++i) {
            dataProvider[i][0] = devices.get(i);
        }
        return dataProvider;
    }

    public void setDriver(Device device) throws Exception {

        String url = getConfig().hasTestFarmProvider() ? getTestFarmProviderUrl() : "";
        if (getConfig().isMWeb()) url = Configuration.APPIUM_URL;

        drivers.set(WebDriverFactory.createWebDriver(device, url));
        driverSessionIds.set(getDriverSessionId(getDriver()));
    }

    public WebDriver getDriver() {
        return drivers.get();
    }

    public String getDriverSessionId() {
        return driverSessionIds.get();
    }

    public String getDriverSessionId(WebDriver driver) {

        String sessionId;
        if (driver instanceof SafariDriver) {
            sessionId = UUID.randomUUID().toString();
        } else {
            sessionId = ((RemoteWebDriver) driver).getSessionId().toString();
        }

        return sessionId;
    }

    public void setCurrentDevice(Device device) {
        currentDevices.set(device);
    }

    public Device getCurrentDevice() {
        return currentDevices.get();
    }

    private String getTestFarmProviderUrl() throws TestManagerException {

        String testFarmUrl;

        switch (getConfig().getTestFarmProviderName()) {
            case "SAUCELABS":
                testFarmUrl = Configuration.SAUCE_LABS_URL;
                break;
            default:
                throw new TestManagerException("No valid test farm url found.");
        }

        return "http://" + getConfig().getTestFarmUser()
                + ":" + getConfig().getTestFarmPassword()
                + "@" + testFarmUrl;
    }

    /**
     * Get SauceLabs public link for current job
     */
    public static String getSauceLabsJobLink() throws TestManagerException {
        return getSauceRestApi().getPublicJobLink(TestManager.getInstance().getDriverSessionId());
    }

    /**
     * Get SauceLabs REST API (stop job, get job info ...)
     */
    public static SauceREST getSauceRestApi() throws TestManagerException {
        String[] credentials = TestManager.getInstance().getTestFarmCredentials();

        if (credentials[0].equals("SAUCELABS")) {
            return new SauceREST(credentials[1], credentials[2]);
        }
        throw new TestManagerException("Cannot find SauceLabs credentials.");
    }

    public String[] getTestFarmCredentials() {
        return new String[]{getConfig().getTestFarmProviderName(), getConfig().getTestFarmUser(), getConfig().getTestFarmPassword()};
    }

    public void open(String url) {
        if (getDriver() == null) {
            try {
                TestManager.getInstance().setDriver(TestManager.getInstance().getCurrentDevice());
            } catch (Exception e) {
                throw new TestException("Cannot create WebDriver for device: " + TestManager.getInstance().getCurrentDevice());
            }
        }
        getDriver().get(url);
    }

    public void open() {
        open(getConfig().getUrl());
    }
}
