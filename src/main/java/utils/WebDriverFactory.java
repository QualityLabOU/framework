package utils;

import io.appium.java_client.remote.MobileCapabilityType;
import models.Device;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.URL;

public class WebDriverFactory {

    private static final Logger LOG = LogManager.getLogger(WebDriverFactory.class);

    public static WebDriver createWebDriver(Device device, String remoteAddress) throws Exception {
        WebDriver driver;

        try {
            if (StringUtils.isNotBlank(remoteAddress)) {

                DesiredCapabilities caps;

                if (device.hasAndroidPlatform() || device.hasIOSPlatform())
                    caps = initMWebCapabilities(device);
                else caps = initWebCapabilities(device);

                driver = new RemoteWebDriver(new URL(remoteAddress), caps);
            } else {
                driver = getLocalDesktopWebDriver(device.getBrowserName());
            }

        } catch (Exception e) {

            LOG.error("Failed to create WebDriver", e);
            throw e;
        }

        return driver;
    }

    private static DesiredCapabilities initMWebCapabilities(Device device) {

        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, Configuration.APPIUM_VERSION);
        if (device.hasName()) {
            capabilities.setCapability("name", device.getName());
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, device.getName());
        }
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, device.getPlatformName());
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, device.getPlatformVersion());
        capabilities.setCapability("deviceOrientation", device.getDeviceOrientation());
        if (!device.getDeviceType().isEmpty()) capabilities.setCapability("deviceType", device.getDeviceType());

        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, device.getBrowserName());


        setOptionalParameters(capabilities, device);


        if (device.getId() != null)
            capabilities.setCapability(MobileCapabilityType.UDID, device.getId());

        LOG.info("CAPABILITIES: " + capabilities.toString());
        return capabilities;
    }


    private static DesiredCapabilities initWebCapabilities(Device device) {
        DesiredCapabilities capabilities;

        switch (device.getBrowserName()) {
            case "CHROME":
                capabilities = DesiredCapabilities.chrome();
                break;
            case "FIREFOX":
                capabilities = DesiredCapabilities.firefox();
                break;
            case "SAFARI":
                capabilities = DesiredCapabilities.safari();
                break;
            case "IE":
                capabilities = DesiredCapabilities.internetExplorer();
                break;
            default:
                capabilities = new DesiredCapabilities();
        }

        if (device.hasName())
            capabilities.setCapability("name", device.getName());

        capabilities.setCapability(CapabilityType.PLATFORM,
                device.getPlatformName() + " " + device.getPlatformVersion());
        capabilities.setCapability(CapabilityType.VERSION, device.getBrowserVersion());

        setOptionalParameters(capabilities, device);

        LOG.info("CAPABILITIES: " + capabilities.toString());
        return capabilities;
    }


    private static WebDriver getLocalDesktopWebDriver(String browser) throws Exception {
        switch (browser) {
            case "SAFARI":
                return new SafariDriver();
            case "CHROME":
                return new ChromeDriver();
            case "FIREFOX":
                return new FirefoxDriver();
            case "IE":
                return new InternetExplorerDriver();
            default:
                throw new Exception("No browser added.");
        }
    }

    public static void setOptionalParameters(DesiredCapabilities capabilities, Device device) {

        /** More info about optional parameters https://wiki.saucelabs.com/display/DOCS/Test+Configuration+Options#TestConfigurationOptions-OptionalSauceTestingFeatures */

        if (device.getRecordVideo() != null) {
            capabilities.setCapability("recordVideo", device.getRecordVideo());
        }
        if (device.getRecordScreenshots() != null) {
            capabilities.setCapability("recordScreenshots", device.getRecordScreenshots());
        }
        if (device.getRecordLogs() != null) {
            capabilities.setCapability("recordLogs", device.getRecordLogs());
        }
        if (device.getCaputeHtml() != null) {
            capabilities.setCapability("captureHtml", device.getCaputeHtml());
        }
        if (device.getAcceptCertificate() != null) {
            capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, device.getAcceptCertificate());
        }
        if (device.getTunnelIdentifier() != null) {
            capabilities.setCapability("tunnelIdentifier", device.getTunnelIdentifier());
        }

    }
}
