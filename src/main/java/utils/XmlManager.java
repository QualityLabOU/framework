package utils;

import models.Config;
import models.Device;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class XmlManager {

    private static final Logger LOG = LogManager.getLogger(XmlManager.class);

    //region System properties
    private static final String SYSTEM_PROPERTY_XML_FILE_CONFIG = "config-xml";
    private static final String SYSTEM_PROPERTY_XML_FILE_DEVICES = "devices-xml";
    //endregion

    //region Config tags
    private static final String TAG_CONFIG = "config";
    private static final String TAG_CONFIG_NAME = "name";
    private static final String TAG_CONFIG_TYPE = "type";
    private static final String TAG_CONFIG_URL = "url";
    private static final String TAG_CONFIG_VERSION_URL = "versionUrl";
    private static final String TAG_CONFIG_TEST_FARM_PROVIDER = "testFarmProvider";
    private static final String TAG_CONFIG_TEST_FARM_USER = "testFarmUser";
    private static final String TAG_CONFIG_TEST_FARM_PASSWORD = "testFarmPassword";
    private static final String TAG_CONFIG_REPORTER_PROJECT_ID = "reporterProjectId";
    private static final String TAG_CONFIG_REPORTER_ACCESS_KEY = "reporterAccessKey";
    private static final String TAG_CONFIG_DEVICES_PER_RUN = "devicesPerRun";
    private static final String TAG_CONFIG_REPORT_CHANNEL_TYPE = "reportChannelType";
    private static final String TAG_CONFIG_REPORT_RECEIVER_TOKEN = "reportReceiverToken";
    private static final String TAG_CONFIG_REPORT_RECEIVER_CHANNEL = "reportReceiverChannel";
    //endregion

    //region Device tags
    private static final String TAG_DEVICE = "device";
    private static final String TAG_DEVICE_NAME = "name";
    private static final String TAG_DEVICE_PLATFORM = "platform";
    private static final String TAG_DEVICE_PLATFORM_VERSION = "platformVersion";
    private static final String TAG_DEVICE_BROWSER = "browser";
    private static final String TAG_DEVICE_BROWSER_VERSION = "browserVersion";
    private static final String TAG_DEVICE_ID = "id";
    private static final String TAG_DEVICE_ORIENTATION = "deviceOrientation";
    private static final String TAG_DEVICE_TYPE = "deviceType";

    // Optional parameters
    private static final String TAG_RECORD_VIDEO = "recordVideo";
    private static final String TAG_RECORD_SCREENSHOTS = "recordScreenshots";
    private static final String TAG_RECORD_LOGS = "recordLogs";
    private static final String TAG_CAPTURE_HTML = "captureHtml";
    private static final String TAG_ACCEPT_CERTIFICATE = "acceptCertificate";
    private static final String TAG_TUNNEL_IDENTIFIER = "tunnelIdentifier";
    //endregion

    public static List<Config> parseConfigs() throws Exception {

        String configXml = System.getProperty(SYSTEM_PROPERTY_XML_FILE_CONFIG, Configuration.DEFAULT_XML_FILE_CONFIG);

        List<Config> configs = new ArrayList<>();
        NodeList nodeList = getNodeListByTag(TAG_CONFIG, configXml);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node dNode = nodeList.item(i);

            if (dNode instanceof Element) {
                Config config = new Config();

                NodeList cNodes = dNode.getChildNodes();
                for (int j = 0; j < cNodes.getLength(); j++) {
                    Node aNode = cNodes.item(j);
                    if (aNode instanceof Element) {
                        String content = (aNode.getLastChild() != null && aNode.getLastChild().getTextContent() != null)
                                ? aNode.getLastChild().getTextContent().trim() : "";
                        switch (aNode.getNodeName()) {
                            case TAG_CONFIG_NAME:
                                config.setName(content);
                                break;
                            case TAG_CONFIG_TYPE:
                                config.setType(content);
                                break;
                            case TAG_CONFIG_URL:
                                config.setUrl(content);
                                break;
                            case TAG_CONFIG_TEST_FARM_PROVIDER:
                                config.setTestFarmProvider(content);
                                break;
                            case TAG_CONFIG_TEST_FARM_USER:
                                config.setTestFarmUser(content);
                                break;
                            case TAG_CONFIG_TEST_FARM_PASSWORD:
                                config.setTestFarmPassword(content);
                                break;
                            case TAG_CONFIG_DEVICES_PER_RUN:
                                config.setDevicesPerRun(content);
                                break;
                            case TAG_CONFIG_VERSION_URL:
                                config.setVersionUrl(content);
                                break;
                        }
                    }
                }

                configs.add(config);
            }

        }

        LOG.info("PARSER CONFIGS: " + configs.size());
        return configs;
    }

    public static List<Device> parseDevices() throws Exception {
        String devicesXml = System.getProperty(SYSTEM_PROPERTY_XML_FILE_DEVICES, Configuration.DEFAULT_XML_FILE_DEVICES);

        List<Device> devices = new ArrayList<>();
        NodeList nodeList = getNodeListByTag(TAG_DEVICE, devicesXml);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node dNode = nodeList.item(i);
            if (dNode instanceof Element) {
                Device device = new Device();

                NodeList cNodes = dNode.getChildNodes();
                for (int j = 0; j < cNodes.getLength(); j++) {
                    Node aNode = cNodes.item(j);
                    if (aNode instanceof Element) {
                        String content = (aNode.getLastChild() != null && aNode.getLastChild().getTextContent() != null)
                                ? aNode.getLastChild().getTextContent().trim() : "";
                        switch (aNode.getNodeName()) {
                            case TAG_DEVICE_NAME:
                                device.setName(content);
                                break;
                            case TAG_DEVICE_PLATFORM:
                                device.setPlatform(content);
                                break;
                            case TAG_DEVICE_PLATFORM_VERSION:
                                device.setPlatformVersion(content);
                                break;
                            case TAG_DEVICE_BROWSER:
                                device.setBrowser(content);
                                break;
                            case TAG_DEVICE_BROWSER_VERSION:
                                device.setBrowserVersion(content);
                                break;
                            case TAG_DEVICE_ID:
                                device.setId(content);
                                break;
                            case TAG_RECORD_VIDEO:
                                device.setRecordVideo(content);
                                break;
                            case TAG_RECORD_SCREENSHOTS:
                                device.setRecordScreenshots(content);
                                break;
                            case TAG_RECORD_LOGS:
                                device.setRecordLogs(content);
                                break;
                            case TAG_CAPTURE_HTML:
                                device.setCaputeHtml(content);
                                break;
                            case TAG_DEVICE_ORIENTATION:
                                device.setDeviceOrientation(content);
                                break;
                            case TAG_DEVICE_TYPE:
                                device.setDeviceType(content);
                                break;
                            case TAG_ACCEPT_CERTIFICATE:
                                device.setAcceptCertificate(content);
                                break;
                            case TAG_TUNNEL_IDENTIFIER:
                                device.setTunnelIdentifier(content);
                                break;
                        }

                    }
                }
                devices.add(device);
            }

        }

        LOG.info("PARSER DEVICES: " + devices.size());
        return devices;
    }

    private static Document getDocument(String xmlName) throws Exception {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        return builder.parse(getXmlFile(xmlName));
    }

    private static NodeList getNodeListByTag(String xmlTag, String xmlName)
            throws Exception {

        Document document = getDocument(xmlName);
        return document.getDocumentElement().getElementsByTagName(xmlTag);
    }

    private static NodeList getChildNodes(String xmlName)
            throws Exception {

        Document document = getDocument(xmlName);
        return document.getDocumentElement().getChildNodes();
    }


    private static File getXmlFile(String xmlName) throws NullPointerException {
        try {
            return new File(XmlManager.class.getClassLoader().getResource(xmlName).getFile());
        } catch (NullPointerException ex) {
            throw new NullPointerException(xmlName + " is missing in 'resources' package");
        }
    }
}