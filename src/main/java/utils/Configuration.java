package utils;

public class Configuration {

    public static final String DEFAULT_CONFIG_NAME = "Default";

    //region XML file names
    /**
     * Default name for device list xml file
     */
    public static final String DEFAULT_XML_FILE_DEVICES = "devices.xml";
    /**
     * Default name for test configurations xml file
     */
    public static final String DEFAULT_XML_FILE_CONFIG = "config.xml";
    //endregion

    public static final String APPIUM_URL = "http://127.0.0.1:4723/wd/hub";
    public static final String APPIUM_VERSION = "1.5";

    public static final String SAUCE_LABS_TEST_FARM_ID = "sauce";
    public static final String SAUCE_LABS_FILE_PATH = "sauce-storage";
    public static final String SAUCE_LABS_URL = "ondemand.saucelabs.com:80/wd/hub";
    public static final String SAUCE_LABS_LINK_URL = "https://saucelabs.com/jobs/";

}
