package utils;

public class TestManagerException extends Exception{

    public TestManagerException(String message) { super(message); }
}
